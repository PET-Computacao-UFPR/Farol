from bottle import Bottle, template, static_file, request, run, redirect
from model import provas_get_functions as Provas
from sessao import main as Sessao
import os
import json
from datetime import datetime
from werkzeug import secure_filename
import unidecode

def init_routes(_app):
	app = _app
	@app.route('/upload')
	def upload():
		if Sessao.get_session():
			labels = Provas.getAllLabels()
			return template("upload/client/index.html",labels=labels)
		else:
			redirect('/login')
	@app.route('/upload/js/<arq>.js')
	def js(arq):
		return static_file(arq+".js","upload/client/js/")

	@app.route('/upload/css/<arq>.css')
	def js(arq):
		return static_file(arq+".css","upload/client/css/")
		
	@app.route('/upload', method='POST')
	def do_upload():
		if Sessao.get_session():
			labels = request.forms.get('labels')
			listLabels = labels.split(',')
			print listLabels

			if labels != '':
				listLabels = list(set(listLabels))
				allLabels = Provas.getAllLabels()
				for l in listLabels:
					if not l in allLabels:
						print "Label invalida"
						return "err_labelInv"
			else:
				listLabels = []

			professor = request.forms.get('professor')
			print professor
			disciplina = request.forms.get('disciplina')
			print disciplina
			ano = request.forms.get('ano')
			print ano
			tipo = request.forms.get('tipo')
			print tipo
			numeroDoc = request.forms.get('numeroDoc')
			print numeroDoc
			if numeroDoc == '':
				numeroDoc = 'x'
			else:
				if tipo == 'prova':
					numeroDoc = 'p'+numeroDoc
				else:
					numeroDoc = 't'+numeroDoc

			numeroDoc = numeroDoc[:3]
			if len(numeroDoc) > 2 and numeroDoc[2].isdigit() == False:
				numeroDoc = numeroDoc[:2] + ""


			documento = []
			j = 0
			while True:
				# print j
				try:
					documento.append(request.files.get('documento_'+str(j)))
				except ValueError:
					print "Erro no request.files.get"
					return "err_getFile"
				j += 1
				if request.files.get('documento_'+`j`) is None:
					break

			if j > 5:
				print "numero de arquivos maximos excedido"
				return "err_maxArqs"

			for i in documento:
				if not i.filename.split('.')[-1] in ['png','pdf','jpg']:
					print "Formato de arquivo nao suportado"
					return "err_format"
				i.filename = 'provisorio'+secure_filename(i.filename)
			
			MAX_SIZE = 20 * 1024 * 1024 # 20MB
			BUF_SIZE = 8192

			strArqNomes = ""
			# code assumes that MAX_SIZE >= BUF_SIZE
			for i in documento:
				data_blocks = []
				byte_count = 0

				buf = i.file.read(BUF_SIZE)
				while buf:
					byte_count += len(buf)
					if byte_count > MAX_SIZE:
						break

					data_blocks.append(buf)
					buf = i.file.read(BUF_SIZE)
					# print "byte_count=" + `byte_count`
					# os.system('echo '+`byte_count`+'>>'+'teste.txt')

				data = ''.join(data_blocks)
				if not buf:
					with open(os.path.join("tmp", i.filename), 'w') as f:
						f.write(data)
				else:
					for k in documento:
						os.system("rm -f tmp/"+k.filename[:-4]+"*")
					print "Arquivo > 20MB"
					return "err_tamArq"

				strArqNomes += "tmp/" + i.filename[:-4] + ".pdf "
				if i.filename.split('.')[-1] != 'pdf':
					print "convertendo..."
					ret = os.system("convert tmp/"+ i.filename + " tmp/" + i.filename[:-4] + ".pdf && rm tmp/" + i.filename)
					if ret != 0:
						for k in documento:
							os.system("rm -f tmp/"+k.filename[:-4]+"*")
						return "err_convert";
					print "OK"

			time = str(datetime.now()).replace(" ","_").replace(":","_")

			prova = {}
			prova['data_upload'] = time
			prova['usuario'] = Sessao.get_session()
			prova['url'] = "tmp/cmprs"+time+".pdf"
			prova['professor'] = unidecode.unidecode(professor.decode("utf-8"))
			prova['disciplina'] = unidecode.unidecode(disciplina.decode("utf-8"))
			prova['ano'] = unidecode.unidecode(ano.decode("utf-8"))
			prova['tipo'] = unidecode.unidecode(tipo.decode("utf-8"))
			prova['numeroDoc'] = unidecode.unidecode(numeroDoc.decode("utf-8"))

			# print "pdfunite "+ strArqNomes + "tmp/"+ time
			if j > 1:
				print "Unindo pdfs..."
				os.system("pdfunite "+ strArqNomes + "tmp/"+ time+".pdf && rm " + strArqNomes)
				print "OK"
			else:
				os.system("mv "+ strArqNomes + "tmp/"+ time+".pdf")

			print "Comprimindo pdfs..."
			os.system("gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile=tmp/cmprs"+time+".pdf tmp/"+time+".pdf && rm tmp/"+time+".pdf")
			print "OK"

			Provas.insereProva(prova,listLabels)
			return "env"

		else:
			print "Nao logado"
			return "err_login"
