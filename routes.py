from bottle import Bottle, template, static_file, request
from busca import routes as busca_routes
from upload import routes as upload_routes
from login import routes as login_routes
from moderacao import routes as moderacao_routes
from moderacaoAdmin import routes as moderacaoAdmin_routes

app = False


def init_routes(_app):
	app = _app

	busca_routes.init_routes(app)
	upload_routes.init_routes(app)
	login_routes.init_routes(app)
	moderacao_routes.init_routes(app)
	moderacaoAdmin_routes.init_routes(app)
	
	#~ @app.route('/hello/<name>')
	#~ def index(name):
		#~ return template('<b>Hello {{name}}</b>!', name=name)
