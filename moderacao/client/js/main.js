function aceita_prova(id_prova){
	$("#prova_line_tr_"+id_prova).remove();
	
	var data = {"id_prova": id_prova};
	var inputProfessor = $("#view_prova_professor").val();
	if(provasContent[id_prova].professor != inputProfessor)
		data["professor"] = inputProfessor;
	var inputAno = $("#view_prova_ano").val();
	if(provasContent[id_prova].ano != inputAno)
		data["ano"] = inputAno;
	var inputDisciplina = $("#view_prova_disciplina").val();
	if(provasContent[id_prova].disciplina != inputDisciplina)
		data["disciplina"] = inputDisciplina;
	var inputNumeroDoc = $("#view_prova_numeroDoc").val();
	if(provasContent[id_prova].numeroDoc != inputNumeroDoc)
		data["numeroDoc"] = inputNumeroDoc;
	var inputTipo = $("#view_prova_tipo").val();
	if(provasContent[id_prova].inputTipo != inputTipo)
		data["tipo"] = inputTipo;
	var view_prova_novasLabels = $("#view_prova_novasLabels").val();
	if(view_prova_novasLabels != "")
		data["novasLabels"] = view_prova_novasLabels;

	$.ajax({
		type: 'POST',
		
		url: 'moderacao/aceitaProva',
		data: data,
		success: function(msg){
			$('#view_prova_disciplina').val("");
			$('#view_prova_ano').val("");
			$('#view_prova_professor').val("");
			$('#view_prova_numeroDoc').val("");
			$('#view_prova_novasLabels').val("");
			$('#view_prova_tipo').val("");

			$('#view_labels_tr').empty();
		}
	});

}

function recusa_prova(id_prova){
	$("#prova_line_tr_"+id_prova).remove();
	var data = {"id_prova": id_prova};
	$.ajax({
		type: 'POST',
		
		url: 'moderacao/deleteProva',
		data: data,
		success: function(msg){
			$('#view_prova_disciplina').val("");
			$('#view_prova_ano').val("");
			$('#view_prova_professor').val("");
			$('#view_prova_numeroDoc').val("");
			$('#view_prova_novasLabels').val("");
			$('#view_prova_tipo').val("");

			$('#view_labels_tr').empty();
		}
	});
	
	
	
}

function abre_fecha(obj){
	if($("#"+obj).css("display")=="none"){
		$("#"+obj).css("display","inline");
	}
	else{
		$("#"+obj).css("display","none");
	}
}

for(l in all_labels_hash){
	var valor = all_labels_hash[l];
	$("#helper_labels_table").append("<tr><td>"+valor+"</td></tr>");
}

for(l in all_professores_hash){
	$("#helper_professores_table").append("<tr><td>"+l+"</td></tr>");
}

for(l in all_disciplinas_hash){
	$("#helper_disciplinas_table").append("<tr><td>"+l+"</td></tr>");
}

var idProvaSelecionada = null;
function selectProva(obj,prova){
	if(parseInt(prova) == parseInt(idProvaSelecionada)){
		return;
	}
	prova = provasContent[prova];
	
	
	$(".prova_line_tr").each(function(i,o){
		$(o).css("outline-width","0px");
		var id_prova = $(o).attr("value");
		$("#provas_pendentes_v_"+id_prova).removeClass("left_selected").addClass("left_actions_prova");
		$("#provas_pendentes_v_"+id_prova).html("");
		$("#provas_pendentes_v_"+id_prova).off('click');
		$("#provas_pendentes_a_"+id_prova).removeClass("selected").addClass("actions_prova");
		$("#provas_pendentes_a_"+id_prova).html("");
		$("#provas_pendentes_a_"+id_prova).off('click');
		$("#provas_pendentes_r_"+id_prova).removeClass("selected").addClass("actions_prova");
		$("#provas_pendentes_r_"+id_prova).html("");
		$("#provas_pendentes_r_"+id_prova).off('click');
	});
	
	var id_prova = $(obj).attr("value");
	idProvaSelecionada = id_prova;
	$("#provas_pendentes_v_"+id_prova).removeClass("left_actions_prova").addClass("left_selected");
	$("#provas_pendentes_v_"+id_prova).html("Visualizar");
	$("#provas_pendentes_v_"+id_prova).click(function(){viewProva(id_prova);});
	$("#provas_pendentes_a_"+id_prova).removeClass("actions_prova").addClass("selected");
	$("#provas_pendentes_a_"+id_prova).html("Aprovar");
	$("#provas_pendentes_a_"+id_prova).click(function(){aceita_prova(id_prova);});
	$("#provas_pendentes_r_"+id_prova).removeClass("actions_prova").addClass("selected");
	$("#provas_pendentes_r_"+id_prova).html("Recusar");
	$("#provas_pendentes_r_"+id_prova).click(function(){recusa_prova(id_prova);});
	
	$(obj).css("outline-color","rgba(255,0,0,0.5)");
	$(obj).css("outline-style","solid");
	$(obj).css("outline-width","1px");
	
	$("#view_prova_disciplina").val(prova.disciplina);
	$("#view_prova_ano").val(prova.ano);
	$("#view_prova_professor").val(prova.professor);
	$("#view_prova_numeroDoc").val(prova.numeroDoc);
	$("#view_prova_tipo").val(prova.tipo);

	var aux = "";
	for(l in prova.labels){
		var valor = all_labels_hash[prova.labels[l]];
		aux+="<tr><td><img src='busca/img/delete.png' onClick=deleteLabel("+"this,"+"\""+valor+"\","+id_prova+");> "+valor+"</td></tr>";
	}
	
	$("#view_labels_tr").html(aux);
	
}

function viewProva(prova){
	prova = provasContent[prova];
	$("#iframe").attr('src',prova.url);
	$("#mostra-prova").css("display","inline");
	switchLight();
	
}

function deleteLabel(elem,nomelabel,id_prova) {
	var data = {};
	data["nomeLabel"] = nomelabel;
	data["id_prova"] = id_prova;
	$.ajax({
	  method: "POST",
	  url: "/moderacao/deleteLabel",
	  data: data
	});

	elem.parentElement.remove();

}


var lightOn = true;
function switchLight(){
	if(lightOn){
		document.getElementById("lightOut").style.display = "inline";
		document.getElementById("mostra-prova").style.display = "inline";
		document.getElementById("buttonLightOn").style.display = "inline";
	}
	else{
		document.getElementById("lightOut").style.display = "none";
		document.getElementById("mostra-prova").style.display = "none";
		document.getElementById("buttonLightOn").style.display = "none";
	}
	lightOn= !lightOn;
}


var input = document.getElementById("view_prova_novasLabels");
var awesomplete = new Awesomplete(input);

var insertLabels = [];

awesomplete.replace = function(text){
	this.input.value = text;
	
	var valor = document.getElementById("view_prova_novasLabels").value;
	
	var data = {};
	data["nomeLabel"] = valor;
	data["id_prova"] = idProvaSelecionada;
	$.ajax({
	  method: "POST",
	  url: "/moderacao/insertLabel",
	  data: data
	});
	
	
	$("#view_labels_tr").html($("#view_labels_tr").html()+"<tr><td><img src='busca/img/delete.png' onClick=deleteLabel("+"this,"+"\""+valor+"\","+idProvaSelecionada+");> "+valor+"</td></tr>");
	
	
	document.getElementById("view_prova_novasLabels").value = " ";
	
	this.close();

};
awesomplete.list = all_labels_hash;


var input = document.getElementById("view_prova_disciplina");
var awesomplete = new Awesomplete(input);
awesomplete.replace = function(text){
	this.input.value = text;

};
awesomplete.list = Object.keys(all_disciplinas_hash);

var input = document.getElementById("view_prova_tipo");
var awesomplete = new Awesomplete(input);
awesomplete.replace = function(text){
	this.input.value = text;

};
awesomplete.list = ["prova","trabalho"];

var input = document.getElementById("view_prova_professor");
var awesomplete = new Awesomplete(input);
awesomplete.replace = function(text){
	this.input.value = text;

};
awesomplete.list = Object.keys(all_professores_hash);
