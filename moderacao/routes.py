from bottle import Bottle, template, static_file, request, run
from model import provas_get_functions as Provas
from model import funcoes_usuario as Usuarios
from model import mv_doc as MvDoc
from sessao import main as Sessao
import os
import json
import datetime
import time
import json

def init_routes(_app):
	app = _app
	@app.route('/moderacao')
	def upload():
		permissao = False
		loginName = Sessao.get_session()
		if(loginName):
			user = Usuarios.getUsuario(loginName)
			if(user['moderador']==1 or user['administrador']==1):
				permissao = True
		
		if permissao:
			provas = Provas.getProvaBy(status=0)
			all_labels = []
			all_disciplinas = {}
			all_professores = {}
			
			provas_db = Provas.getProvaBy()
			for p in provas_db:
				labels_db = Provas.getLabelsFromProva(p['id_documento'])
				
				for l in labels_db:
					if not(l in all_labels):
						all_labels.append(l)
				
				if(p['professor'] in all_professores):
					all_professores[p['professor']]+=1
				else:
					all_professores[p['professor']]=1
				
				if(p['disciplina'] in all_disciplinas):
					all_disciplinas[p['disciplina']]+=1
				else:
					all_disciplinas[p['disciplina']]=1
			
			
			for p in provas:
				labels_prova = []
				labels_db = Provas.getLabelsFromProva(p['id_documento'])
				for l in labels_db:
					index = all_labels.index(l)
					labels_prova.append(index)
					
				p["labels"] = list(labels_prova)
			
			
			
				
			
			#labels = json.dumps(labels, ensure_ascii=False)
			all_labels = json.dumps(all_labels, ensure_ascii=False)
			all_professores = json.dumps(all_professores, ensure_ascii=False)
			all_disciplinas = json.dumps(all_disciplinas, ensure_ascii=False)
			return template("moderacao/client/index.html",
					provas = provas,
					all_labels = all_labels,
					all_professores = all_professores,
					all_disciplinas = all_disciplinas
			)
		
		else:
			return "<p>Nao logado</p>"
	
	@app.route('/moderacao/aceitaProva',method='POST')
	def aceitaProva():
		permissao = False
		loginName = Sessao.get_session()
		if(loginName):
			user = Usuarios.getUsuario(loginName)
			if(user['moderador']==1 or user['administrador']==1):
				permissao = True
		
		if permissao:
			id_prova = int(request.forms.get("id_prova"))
			provas = Provas.getProvaBy(status=0)

			id_ok = 0
			for i in provas:
				if i['id_documento'] == int(id_prova):
					id_ok = 1
					break

			if id_ok == 0:
				return "<p>Erro</p>"
			
			professor = request.forms.get("professor")
			ano = request.forms.get("ano")
			disciplina = request.forms.get("disciplina")
			tipo = request.forms.get("tipo")
			novasLabels = request.forms.get("novasLabels")
			numeroDoc = request.forms.get("numeroDoc")
			
			if tipo != None:
				Provas.mudaTipoDocumento(id_prova,tipo)
			if numeroDoc != None:
				Provas.mudaNumeroDocumento(id_prova,numeroDoc)
			if ano != None:
				Provas.mudaAnoProva(id_prova,ano)
			if professor != None:
				Provas.mudaProfessorProva(id_prova,professor)
			if disciplina != None:
				Provas.mudaDisciplinaProva(id_prova,disciplina)
			if novasLabels != None:
				listNovasLabels = novasLabels.split(',')
				if listNovasLabels[-1] == '':
					del listNovasLabels[-1]

				for i in listNovasLabels:
					Provas.insereLabel(i, [id_prova])

			Provas.mudaStatusProva(id_prova,1)
			MvDoc.ApproveDocument(id_prova)
		else:
			return "<p>Nao logado</p>"

	@app.route('/moderacao/deleteLabel',method='POST')
	def delLabel():
		permissao = False
		loginName = Sessao.get_session()
		if(loginName):
			user = Usuarios.getUsuario(loginName)
			if(user['moderador']==1 or user['administrador']==1):
				permissao = True
		
		if permissao:
			provas = Provas.getProvaBy(status=0)
			id_prova = request.forms.get("id_prova")

			id_ok = 0
			for i in provas:
				if i['id_documento'] == int(id_prova):
					id_ok = 1
					break

			if id_ok == 0:
				return "<p>Erro</p>"

			Provas.deleteRelacaoLabelDoc(request.forms.get("nomeLabel"),id_prova)
	
	@app.route('/moderacao/insertLabel',method='POST')	
	def insertLabel():
		permissao = False
		loginName = Sessao.get_session()
		if(loginName):
			user = Usuarios.getUsuario(loginName)
			if(user['moderador']==1 or user['administrador']==1):
				permissao = True
		
		if permissao:
			provas = Provas.getProvaBy(status=0)
			id_prova = request.forms.get("id_prova")

			id_ok = 0
			for i in provas:
				if i['id_documento'] == int(id_prova):
					id_ok = 1
					break

			if id_ok == 0:
				return "<p>Erro</p>"

			Provas.relacionaProvaLabelByName(request.forms.get("nomeLabel"),id_prova)
	
	@app.route('/moderacao/deleteProva',method='POST')	
	def deleteProva():
		permissao = False
		loginName = Sessao.get_session()
		if(loginName):
			user = Usuarios.getUsuario(loginName)
			if(user['moderador']==1 or user['administrador']==1):
				permissao = True
		
		if permissao:
			provas = Provas.getProvaBy(status=0)
			id_prova = request.forms.get("id_prova")

			id_ok = 0
			for i in provas:
				if i['id_documento'] == int(id_prova):
					id_ok = 1
					break

			print "id_ok = "+str(id_ok)

			if id_ok == 0:
				return "<p>Erro</p>"

			MvDoc.removeDoc(request.forms.get("id_prova"))
			Provas.deleteProva(request.forms.get("id_prova"))

	@app.route('/moderacao/css/<arq>.css')
	def css(arq):
		return static_file(arq+".css","moderacao/client/css/")
		
	@app.route('/moderacao/js/<arq>.js')
	def css(arq):
		return static_file(arq+".js","moderacao/client/js/")
