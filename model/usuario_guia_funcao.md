## getUsuario
Função que retorna um dicionário com os campos login,nome,curso,status moderador e status administrador. caso nao seja encontrado nenhum usuario retornara None.
### Parâmetro
Uma string contendo o login.

## getAllUsuario
Função que retorna uma lista de dicionario com os mesmo campos da funcao getUsuario.
## createUsuario
Cria no banco de dados o usuário.
Se o login ou email existe a funcao nao cria o novo usuario, verificar previamente se o login ou email ja está no banco.
Status de moderador e administrador será por padrão falso.
### Parâmetro
Dicionário com os campos nome,login,email,senha e curso.
## updateSenha
Atualiza o campo senha no banco de dados para o login inserido.
### Parâmetro
Dicionário com os campos login e senha.
## authentication
Retorna true ou false se a senha passada como parametro corresponde a senha que está no banco de dados para o login passado.
### Parâmetro
Dicionário com o campo login e senha.
## getUsuarioEmail 
Retorna um dicionário com os campos nome,curso,login,email,status de moderador e administrador. Se o a função não achar um usuário a função retorna None
### Parâmetro
String email.
## updateUsuario
Atualiza no banco de dados o campo em que a informação do dicionário difere da informação contida no banco de dados. por exemplo se quer alterar o nome de um usuario passe um dicionário com o nome alterado e toda as outras informacões iguais.A função não altera o campo senha.
### Parâmetro
Dicionário com os campos nome,login,email,curso,status de moderador e administrador( true ou false).