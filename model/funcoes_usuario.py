import unicodedata
import bcrypt
import string
import sqlite3

#funcao que cria usuario
def createUsuario(usuario):
     con = sqlite3.connect('data.db')
     c = con.cursor()
     login_existe = c.execute('SELECT login from usuario where login=?',(usuario['login'],) ).fetchall() #verifica se o login ja existe
     email_existe = c.execute('SELECT email from usuario where email=?',(usuario['email'],) ).fetchall() #verifica se o email ja existe
     if (len(login_existe) == 0) and (len(email_existe) == 0):
         hash_password = bcrypt.hashpw(usuario['senha'].encode('utf-8'), bcrypt.gensalt())
         query = "INSERT INTO usuario (nome, email, login, senha, administrador, moderador, curso) VALUES (?,?,?,?,?,?,?)"
         c.execute(query, (usuario['nome'],usuario['email'],usuario['login'],hash_password,usuario['admin'],usuario['mod'],usuario['curso']) )
     con.commit()

     con.close()
#funcao que compara uma senha com uma senha que esta no banco de dados. funcao retorna um valor booleano.
def authentication(usuario):
    con = sqlite3.connect('data.db')
    c = con.cursor()
    login_db = c.execute('SELECT login from usuario where login=?',(usuario['login'],)).fetchone()
    if(login_db):
		login_db = login_db[0]

    if not(login_db is None):
        password_db = c.execute('SELECT senha from usuario where login=?',(login_db,)).fetchone()
        con.close()
        password_db = (unicodedata.normalize('NFKD', str(password_db[0]).decode('utf-8')).encode('ascii','ignore'))
        if (bcrypt.hashpw(usuario['senha'].encode('utf-8'), password_db)) == password_db:
            con.close()
            return login_db
        else:
            con.close()
            return False

    con.close()
    return False

#funcao que retorna uma lista de dicionario com todos os usuarios
def getAllUsuario():
    con = sqlite3.connect('data.db')
    c = con.cursor()
    allusers = c.execute('SELECT * FROM usuario').fetchall()
    allusers = list(allusers)
    allusuario = []
    for i in range(len(allusers)):
        allusuario.append( {'nome':allusers[i][1],'email':allusers[i][2],'login':allusers[i][3],'administrador':allusers[i][5],'moderador':allusers[i][6],'curso':allusers[i][7]})
    con.close()
    return allusuario

 #funcao que retorna os dados de um usuario,se ele nao for encontrado retorna None
def getUsuario(valor):
    con = sqlite3.connect('data.db')
    c = con.cursor()
    users = c.execute('SELECT * from usuario where login=?', (valor,)).fetchall()
    users = list(users)
    if len(users) == 1:
        user = {'nome':users[0][1],'email':users[0][2],'login':users[0][3],'administrador':users[0][5],'moderador':users[0][6],'curso':users[0][7]}
        con.close()
        return user
    con.close()

def updateUsuario(usuario):
    con = sqlite3.connect('data.db')
    c = con.cursor()
    usuario_db=getUsuario(usuario['login'])
    if usuario['nome'] != usuario_db['nome']: #edita o nome
        c.execute('update usuario SET nome = ? where login = ?', ( usuario['nome'], usuario['login'],))
    if usuario['curso'] != usuario_db['curso']: #edita o curso
        c.execute('update usuario SET curso = ? where login = ?', ( usuario['curso'], usuario['login'],))
    if usuario['email'] != usuario_db['email']: #edita o email
        c.execute('update usuario SET email = ? where login = ?', ( usuario['email'], usuario['login'],))
    if usuario['moderador']: #edita o status de moderador
        if usuario_db['moderador']==0:
            c.execute('update usuario SET moderador = ? where login = ?', ( 1, usuario['login'],))
    else:
        if usuario_db['moderador']==1:
            c.execute('update usuario SET moderador = ? where login = ?', ( 0, usuario['login'],))

    if usuario['administrador']: #edita o status de administrador
        if usuario_db['administrador']==0:
            c.execute('update usuario SET administrador = ? where login = ?', ( 1, usuario['login'],))
    else:
        if usuario_db['administrador']==1:
            c.execute('update usuario SET administrador = ? where login = ?', ( 0, usuario['login'],))
    con.commit()
    con.close()
def updateSenha(usuario):
    con = sqlite3.connect('data.db')
    c = con.cursor()
    hash_password = bcrypt.hashpw(usuario['senha'], bcrypt.gensalt())
    c.execute('update usuario SET senha = ? where login = ?', (hash_password, usuario['login'],))
    con.commit()
    con.close()
#obtem usuario com o email fornecido, caso contrario retorna None
def getUsuarioEmail(Email):
    con = sqlite3.connect('data.db')
    c = con.cursor()
    users = c.execute('SELECT * from usuario where email=?', (Email,)).fetchall()
    users = list(users)
    if len(users) == 1:
        user = {'nome':users[0][1],'email':users[0][2],'login':users[0][3],'administrador':users[0][5],'moderador':users[0][6],'curso':users[0][7]}
        con.close()
        return user
    con.close()
