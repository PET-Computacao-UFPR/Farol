from datetime import datetime
from provas_get_functions import *
import os
import shutil

#move um documento da pasta tmp para pasta documento
def ApproveDocument(id):
        
        doc = getProvaBy(id_documento=id)[0]
        label = doc['numeroDoc']
        
        src = doc['url']
        disciplina = doc['disciplina'].replace(" ","-")
        professor = doc['professor'].replace(" ","-")
        #~ tipo = doc['url'].split('.')
        #~ tipo = tipo[len(tipo)-1] # extensao do arquivo
        tipo = "pdf"
        nome = label +"-" + doc['data_upload']+"."+tipo # nome do arquivo
        to = "documentos/"+doc['tipo']+"/"+disciplina+"/"+professor+"/"+doc['ano']+"/"
        if not os.path.exists(to): # caso o diretorio destino nao exista
                os.makedirs(to)
        shutil.move(os.path.abspath(src),os.path.abspath(to+nome))
    #    mudaStatusProva(id,1) 
        url = to + nome
        mudaUrlProva(id,url)

#TODO: mover para diretorio lixo quando documento for deletado

def removeDoc(id):
    doc = getProvaBy(id_documento=id)[0]
    lixo = "lixo"

    if not os.path.exists(lixo):
            os.makedirs(lixo)
    
    src = doc['url']
    shutil.move(src,lixo)






