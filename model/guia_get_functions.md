#getProvaBy(id_prova, id_usuario, professor, disciplina, ano, tipo, [id_labelA,id_labelB,id_labelC...] )

Com o parâmetro None o campo será desconsiderado.
Retorna uma lista provas(dicionários).

#getIdLabels([labelA,labelB,labelC...])

Retorna lista com os ids.

#getProvasFromLabels([labelA,labelB,labelC...])

Retorna as provas associadas as labels.

#getLabelsFromProva(id_prova)

Retorna lista de labels associadas a prova.

#insereLabel(label, [id_provaA, id_provaB, id_provaC...])

Insere label e a associa as provas na lista.

#relacionaProvaLabel(id_prova, id_label)

Relaciona prova com label.

#insereProva({'data_upload': 'dd/mm/aaaa' ,'usuario': '~','url': '~', 'professor': '~','disciplina': '~', 'ano': 'aaaa','tipo': '~'}, [labelA,labelB,labelC...] )

Insere prova.






