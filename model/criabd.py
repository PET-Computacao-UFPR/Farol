import sqlite3

con = sqlite3.connect('data.db')
c = con.cursor()

c.execute("CREATE TABLE label ( id_label integer primary key, nome varchar(255) not null UNIQUE, status boolean not null )")

c.execute("CREATE TABLE usuario ( id_usuario integer primary key, nome varchar(255) not null UNIQUE, email varchar(255) not null UNIQUE, login varchar(255) not null UNIQUE, senha varchar(255) not null, administrador boolean not null, moderador boolean not null, curso varchar(255) not null )")

c.execute("CREATE TABLE documento( id_documento INTEGER PRIMARY KEY,data_upload VARCHAR(255) NOT NULL, status BOOLEAN NOT NULL, usuario VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, professor VARCHAR(255) NOT NULL, disciplina VARCHAR(255) NOT NULL, ano VARCHAR(255), tipo VARCHAR(255), numeroDoc VARCHAR(3) NOT NULL, FOREIGN KEY(usuario) REFERENCES usuario(id_usuario) ) ")

c.execute("CREATE TABLE label_documento( id_label integer not null, id_documento integer not null, FOREIGN KEY(id_documento) REFERENCES documento(id_documento) ON DELETE CASCADE )")