import sqlite3
from itertools import chain

def chunkIt(seq, num):
  ret = []
  while len(seq) > 0:
  	aux = []
  	for i in range(num):
  		aux.append(seq[0])
  		del seq[0]
  	ret.append(aux)

  return ret

def getProvaBy(id_documento=None,id_usuario=None,professor=None,disciplina=None,ano=None,tipo=None,id_label=None,numeroDoc=None,status=None):
	con = sqlite3.connect('data.db')
	c = con.cursor();

	param = []
	stment = "SELECT * FROM documento WHERE "
	if(id_documento != None):
		stment += "id_documento=? AND  "
		param.append(id_documento)
	if(id_usuario != None):
		stment += "usuario=? AND  "
		param.append(id_usuario)
	if(professor != None):
		stment += "professor=? AND  "
		param.append(professor)
	if(disciplina != None):
		stment += "disciplina=? AND  "
		param.append(disciplina)
	if(ano != None):
		stment += "ano=? AND  "
		param.append(ano)
	if(status != None):
		stment += "status=? AND  "
		param.append(status)
	if(tipo != None):
		stment += "tipo=? AND  "
		param.append(tipo)
	if(status != None):
		stment += "status=? AND  "
		param.append(status)
	if(numeroDoc != None):
		stment += "numeroDoc=? AND  "
		param.append(numeroDoc)

	if(id_label != None):
		query = "SELECT * FROM label_documento WHERE"
		lstIdsLabel = []
		if(type(id_label) is list):
			lstIdsLabel.extend(id_label)
			while(id_label):
				query += " id_label=? OR"
				del id_label[0]
		else:
			lstIdsLabel.append(id_label)
			query += " id_label=? OR"
		
		c.execute( query[:-2], lstIdsLabel )
		idsLine = list(chain.from_iterable(c.fetchall()))

		stmentAux = "( "
		idsDoc = []
		for i in range(1,len(idsLine),2):
			stmentAux += "id_documento=? OR   "
			idsDoc.append(idsLine[i])

		stment = stment + stmentAux[:-6]+")      "
		param = param + idsDoc
		
	
	c.execute(stment[:-6], param )

	data = c.fetchall()
	lst = list(chain.from_iterable(data))
	
	provas = []
	prova = {}

	for i in chunkIt(lst,10):
		prova['id_documento'] = i[0]
		prova['data_upload'] = i[1]
		prova['status'] = i[2]
		prova['usuario'] = i[3]
		prova['url'] = i[4]
		prova['professor'] = i[5]
		prova['disciplina'] = i[6]
		prova['ano'] = i[7]
		prova['tipo'] = i[8]
		prova['numeroDoc'] = i[9]
		provas.append(prova.copy())

	con.commit()
	con.close()

	return provas

def getIdLabels(labels):
	con = sqlite3.connect('data.db')
	c = con.cursor()

	query = "SELECT id_label FROM label WHERE"
	for i in labels:
		 query += " nome=? OR"

	ids = list(chain.from_iterable(c.execute(query[:-2],labels).fetchall()))

	con.commit()
	con.close()

	return ids

def getProvasFromLabels(labels):
	return getProvaBy(None,None,None,None,None,None,getIdLabels(labels),None)

def getLabelsFromProva(id_prova):
	con = sqlite3.connect('data.db')
	c = con.cursor()

	id_labels = list(chain.from_iterable(c.execute('SELECT id_label FROM label_documento WHERE id_documento=?',(id_prova,) ).fetchall()))
	if(len(id_labels) == 0):
		return []
	
	query = "SELECT nome FROM label WHERE"
	for i in id_labels:
		query += " id_label="+str(i)+" OR"


	labels = list(chain.from_iterable(c.execute(query[:-2],)))

	con.commit()
	con.close()
	
	return labels

def insereLabel(label, id_provas):
	con = sqlite3.connect('data.db')
	c = con.cursor()

	c.execute("INSERT INTO label (nome,status) VALUES (?, 0)",(label,))
	id_label = c.lastrowid;

	for i in id_provas:
		c.execute("INSERT INTO label_documento (id_label,id_documento) VALUES (?, ?)",(id_label,i))


	con.commit()
	con.close()

def relacionaProvaLabel(id_prova, id_label):
	con = sqlite3.connect('data.db')
	c = con.cursor()

	c.execute("INSERT INTO label_documento (id_label,id_documento) VALUES (?, ?)",(id_label,id_prova))

	con.commit()
	con.close()

def insereProva( prova, label_nome):

	con = sqlite3.connect('data.db')
	c = con.cursor()
	c.execute('INSERT INTO documento (data_upload,status,usuario,url,professor,disciplina,ano,tipo,numeroDoc) VALUES (?,0,?,?,?,?,?,?,?)', (prova['data_upload'], prova['usuario'],prova['url'],prova['professor'], prova['disciplina'], prova['ano'], prova['tipo'], prova['numeroDoc']) )
	id_documento = c.lastrowid


	while(label_nome):
		label = label_nome[0]
		id_label = c.execute('SELECT id_label FROM label WHERE nome=?',(label,) ).fetchall()

		if(len(id_label) == 0):
			c.execute('INSERT INTO label (nome, status) VALUES (?,0)',(label,) )
			id_label = c.lastrowid;
		else:
			id_label = list(id_label[0])[0]

		c.execute('INSERT INTO label_documento (id_label, id_documento) VALUES (?,?)', (id_label, id_documento) )
		del label_nome[0]

	con.commit()
	con.close()

def getAllLabels():
	con = sqlite3.connect('data.db')
	c = con.cursor()
	labels = list(chain.from_iterable(c.execute('SELECT nome FROM label').fetchall()))
	con.commit()
	con.close()

	return labels

def mudaStatusProva(id_prova,status):
	con = sqlite3.connect('data.db')
	c = con.cursor()

	c.execute('UPDATE documento SET status=? WHERE id_documento=?',(status,id_prova) )

	con.commit()
	con.close()


def mudaUrlProva(id_prova,url):
	con = sqlite3.connect('data.db')
	c = con.cursor()
	c.execute('UPDATE documento SET url=? WHERE id_documento=?',(url,id_prova))
	con.commit()
	con.close()
 
def mudaDisciplinaProva(id_prova,disciplina):
	con = sqlite3.connect('data.db')
	c = con.cursor()

	c.execute('UPDATE documento SET disciplina=? WHERE id_documento=?',(disciplina,id_prova))
	con.commit()
	con.close()


def mudaProfessorProva(id_prova,professor):
	con = sqlite3.connect('data.db')
	c = con.cursor()

	c.execute('UPDATE documento SET professor=? WHERE id_documento=?',(professor,id_prova))
	con.commit()
	con.close()
        
def mudaAnoProva(id_prova,ano):
	con = sqlite3.connect('data.db')
	c = con.cursor()

	c.execute('UPDATE documento SET ano=? WHERE id_documento=?',(ano,id_prova))
	con.commit()
	con.close()

def mudaNumeroDocumento(id_prova,numeroDoc):
	con = sqlite3.connect('data.db')
	c = con.cursor()

	c.execute('UPDATE documento SET numeroDoc=? WHERE id_documento=?',(numeroDoc,id_prova))
	con.commit()
	con.close()

def mudaTipoDocumento(id_prova,tipo):
	con = sqlite3.connect('data.db')
	c = con.cursor()

	c.execute('UPDATE documento SET tipo=? WHERE id_documento=?',(tipo,id_prova))
	con.commit()
	con.close()


def deleteRelacaoLabelDoc(nomeLabel,id_documento):
	con = sqlite3.connect('data.db')
	c = con.cursor()

	id_label = list(chain.from_iterable(c.execute('SELECT id_label FROM label WHERE nome=?',(nomeLabel,)).fetchall()))

	c.execute('DELETE FROM label_documento WHERE id_documento=? AND id_label=?',(id_documento,id_label[0]))
	con.commit()
	con.close()

def relacionaProvaLabelByName(nomeLabel,id_documento):
	con = sqlite3.connect('data.db')
	c = con.cursor()
	id_label = list(chain.from_iterable(c.execute('SELECT id_label FROM label WHERE nome=?',(nomeLabel,)).fetchall()))
	c.execute("INSERT INTO label_documento (id_label,id_documento) VALUES (?, ?)",(id_label[0],id_documento))

	con.commit()
	con.close()


def deleteProva(id_prova):
	con = sqlite3.connect('data.db')
	c = con.cursor()
	
	c.execute('PRAGMA foreign_keys = ON')
	c.execute('DELETE FROM documento WHERE id_documento=?',(id_prova,))

	con.commit()
	con.close()


