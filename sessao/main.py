from bottle import Bottle,template,static_file, response, request, abort
from model import funcoes_usuario as user


def try_login(login,passwd):
	usuario = {'login': login, 'senha': passwd}
	login_name = user.authentication(usuario)
	
	if(login_name):
		response.set_cookie("account", str(login_name), secret='some-secret-key')
		
	return login_name
	
def get_session():
	#Retorna nome do usuario (atributo unico na tabela do BD)
	return request.get_cookie("account", secret='some-secret-key')

def logout():
	response.set_cookie("account","",expires=0)
	
