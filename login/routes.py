from bottle import Bottle, template, static_file, request, redirect
from model import funcoes_usuario as user

from sessao import main as Sessao

import json



def init_routes(_app):
	app = _app
	
	
	#Sessao.logout()
		
	
	@app.route('/login')
	def login():
		login = Sessao.get_session()
		
		if(login):
			return template("login/client/index.html",{"logado":True, "login": login})
		
		return template("login/client/index.html",{"logado":False})
	
	@app.route('/login', method='POST')
	def do_login():
		
		doLogout = request.forms.get("logout")
		if(doLogout):
			Sessao.logout()
			return template("login/client/index.html",{"logado":False})
		
		login = Sessao.get_session()
		
		
		login = request.forms.get('username')
		senha = request.forms.get('password')

		if Sessao.try_login(login,senha):
			redirect('/upload')	
			# return template("login/client/index.html",{"logado":True, "login": login})

		else:
			return template("login/client/index.html",{"logado":False})

	@app.route('/logout')
	def logout():
		Sessao.logout()
		redirect('/busca')
	
	@app.route('/admin')
	def admin():
		return static_file("admin.html","login/client/")

	@app.route('/admin', method='POST')
	def setUser():
		nome = request.forms.get('nome')
		email = request.forms.get('email')
		login = request.forms.get('login')
		senha = request.forms.get('senha')
		_admin = request.forms.get('administrador')
		_mod = request.forms.get('moderador')
		if (_admin == 'on'):
			admin = 1
		else:
			admin = 0
		if (_mod == 'on'):
			mod = 1
		else:
			mod = 0
		curso = request.forms.get('curso')

		usuario = {'nome': nome, 'email': email, 'login': login, 'senha': senha, 'admin': admin, 'mod': mod, 'curso': curso}
		user.createUsuario(usuario)
