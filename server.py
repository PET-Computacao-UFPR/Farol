from bottle import Bottle, run

app = Bottle()

import routes

routes.init_routes(app)

run(app, host='0.0.0.0', port=8080)
