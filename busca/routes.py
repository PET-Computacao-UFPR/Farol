from bottle import Bottle,template,static_file
from model import provas_get_functions as Provas
from sessao import main as Sessao
import json
from sessao import main as Sessao
from model import funcoes_usuario as Usuarios

def init_routes(_app):
	app = _app
	@app.route('/busca')
	def busca():
		if Sessao.get_session():
			return template("busca/client/index.html",logado=1)
		else:
			return template("busca/client/index.html",logado=0)			
				
	@app.route('/busca/css/<arq>.css')
	def css(arq):
		return static_file(arq+".css","busca/client/css/")
	@app.route('/busca/js/<arq>.js')
	def js(arq):
		if(arq != "main"):
			return static_file(arq+".js","busca/client/js/")
		else:
			provas = []
			labels = []
			provas_db = Provas.getProvaBy(status=1)
			for p in provas_db:
				labels_prova = []
				labels_db = Provas.getLabelsFromProva(p['id_documento'])
				for l in labels_db:
					if l in labels:
						index = labels.index(l)
						labels_prova.append(index)
					else:
						labels.append(l)
						labels_prova.append(len(labels)-1)

				provas.append({
					"professor":p['professor'],
					"ano":p['ano'],
					"disciplina":p['disciplina'],
					"numeroDoc":p['numeroDoc'],
					"pdfName":p['url'].replace("documentos/",""),
					"labels": labels_prova
					})

			dic = {}
			dic["provas"] = json.dumps(provas, ensure_ascii=False)
			dic["labels"] = json.dumps(labels, ensure_ascii=False)
			
			return template("busca/client/js/main.js",dic)


	@app.route('/busca/img/<arq>.png')
	def imgPng(arq):
		return static_file(arq+".png","busca/client/img/",mimetype="image/png")
	
	@app.route('/documentos/<path:path>')
	def callbackDoc(path):
		return static_file(path, "documentos")
		
	@app.route('/tmp/<path:path>')
	def callbackTmp(path):
		permissao = False
		loginName = Sessao.get_session()
		if(loginName):
			user = Usuarios.getUsuario(loginName)
			if(user['moderador']==1 or user['administrador']==1):
				permissao = True
		
		if permissao:
			return static_file(path, "tmp")
	
	#~ @app.route('/documentos/<arq>.pdf')
	#~ def imgDocumento(arq):
		#~ print arq
		#~ return static_file(arq+".pdf","documentos/",mimetype="application/pdf")

#{"professor":"pr1", "ano":"2000", "disciplina":"d1", "pdfName":"url", "labels": [0,2]}
