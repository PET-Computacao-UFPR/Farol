var filterTags = [];

function delete_filter(i){
	filterTags.splice(i,1);
	updateButtonFilter();
	reload_and_search();
}

function updateButtonFilter(){
	var divFilter = document.getElementById("filters");
	divFilter.innerHTML="";
	for(var i in filterTags){
		divFilter.innerHTML+="<div id=filter> <label>"+filterTags[i]+"</label><button class = button_blue id=XfilterTag"+i+" onClick=delete_filter("+i+");><img src=busca/img/delete.png></button></div>";
	}

}

function reload_and_search(){
	document.getElementById("content").innerHTML="";
	if(filterTags.length == 0) return;
	
	var results = 0;
	//Esta fazendo com base no AND
	
	var id_atual =  0;
	
	data.map(function(prova){
		var add = false;
		var filterTags_copy = filterTags.slice();
		
		for(var i in prova.labels){
			var index = prova.labels[i];
			if( filterTags.indexOf(labels[index]) != -1 ){
				filterTags_copy.splice(filterTags_copy.indexOf(labels[index]),1);
			}
			
			if(filterTags_copy.length == 0){
				break;
			}
		};
		
		if( filterTags.indexOf(prova.professor) != -1 ){
			filterTags_copy.splice(filterTags_copy.indexOf(prova.professor),1);
			console.log(filterTags_copy);
			console.log(filterTags);
		}
		if( filterTags.indexOf(prova.ano) != -1 ){
			filterTags_copy.splice(filterTags_copy.indexOf(prova.ano),1);
		}
		if( filterTags.indexOf(prova.disciplina) != -1 ){
			filterTags_copy.splice(filterTags_copy.indexOf(prova.disciplina),1);
		}
		if( filterTags.indexOf(prova.numeroDoc) != -1 ){
			filterTags_copy.splice(filterTags.indexOf(prova.numeroDoc),1);
		}
		
		
		if(filterTags_copy.length == 0){
			addButton(
				id_atual,
				"content",
				prova.professor+"-"+prova.disciplina+"-"+prova.ano+'-'+prova.numeroDoc,
				"buttonDefault"
			);
			
			results++;
		}
		id_atual++;
		
	});
	
	return results;
	
	
	
}

function updateInputDataSearch(){

	complete_values = []
	
	
	data.map(function(prova){
		if( complete_values.indexOf(prova.professor) == -1 ) //Verifica se ja esta no array
			complete_values.push( prova.professor );
		if( complete_values.indexOf(prova.disciplina) == -1 )
			complete_values.push( prova.disciplina );
		if( complete_values.indexOf(prova.ano) == -1 )
			complete_values.push( prova.ano );
		if( complete_values.indexOf(prova.numeroDoc) == -1 )
			complete_values.push( prova.numeroDoc );
	});
	
	labels.map(function(label){
		if( complete_values.indexOf(label) == -1 )
			complete_values.push(label);
	});
	
	console.log(complete_values);
	complete_values = complete_values.filter( function(x) {return x!=""; } );
	var input = document.getElementById("search_input");
	var awesomplete = new Awesomplete(input);

	awesomplete.replace = function(text){
		this.input.value = text;

		filterTags.push(document.getElementById("search_input").value);
		document.getElementById("search_input").value = " ";
		updateButtonFilter();
		reload_and_search();
		this.close();

	};

	awesomplete.list = complete_values;

}
